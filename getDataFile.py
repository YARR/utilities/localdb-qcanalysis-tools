#!/usr/bin/env python3

from pymongo import MongoClient
from bson.objectid import ObjectId
import os, sys, json, shutil, gridfs, argparse

def connectDB():
    client = MongoClient( port=27017 )
    return client

def getArgs():
    parser = argparse.ArgumentParser( formatter_class=argparse.ArgumentDefaultsHelpFormatter )
    parser.add_argument("--module", "-m", help="module's serial number", type=str)
    parser.add_argument("--stage", "-s", help="stage's name", type=str)
    parser.add_argument("--path", "-p", help="path for data files", type=str, default="db-data" )

    args = parser.parse_args()
    return args

def getDataFile( localdb, userdb, fs, mname, sname, path, commit_doc = None):

    if commit_doc == None:
        commit_doc = userdb.itkpd.uploader.commit.find_one({ "$and":[ { "component_name":mname }, {"stage":sname} ]})

    if commit_doc == None:
        print("This stage are not commited.")
        sys.exit()

    config_file = {
                    "datadir": path,
                    "module":{
                      "serialNumber":mname,
                    },
                    "chip":[]
                  }

    if os.path.exists(path):
        shutil.rmtree(path)

    tmp_dir = "{0}/{1}".format( path, "module_" + mname)

    for i,v in enumerate(commit_doc["results"]):
        query = {"_id": ObjectId(commit_doc["results"][v])}
        tr_doc = localdb.testRun.find_one( query )
        if i == 0:
            config_file["module"]["chipType"] = tr_doc["chipType"]
        this_dir = "{0}/{1}_1".format( sname, tr_doc["testType"] )
        os.makedirs( tmp_dir + "/" + this_dir )
        query = {"testRun": str(tr_doc["_id"])}
        this_ctrs = localdb.componentTestRun.find( query )

        j = 0
        for this_ctr in this_ctrs:
            query = {"name": this_ctr["name"]}
            this_cp = localdb.component.find_one(query)
            if this_cp["componentType"] == "front-end_chip":
                if i == 0:
                    config_file["chip"].append({ "serialNumber":this_cp["name"], "filepath":[] })
                for attachment in this_ctr["attachments"]:
                    data = fs.get(ObjectId(attachment["code"])).read()
                    filename = "{0}_{1}".format( this_cp["name"], attachment["filename"] )
                    filepath = "{0}/{1}/{2}".format( tmp_dir, this_dir, filename )
                    config_file["chip"][j]["filepath"].append(this_dir + "/" + filename)
                    with open(filepath, "wb") as f:
                        f.write(data)
                j += 1

    with open( path + "/info.json", "w") as f:
        json.dump( config_file, f, ensure_ascii=False, indent=4, sort_keys=False, separators=(',', ': ') )

def main():
    client = connectDB()
    localdb = client["localdb"]
    userdb = client["localdbtools"]
    fs = gridfs.GridFS(localdb)
    args = getArgs()
    getDataFile( localdb, userdb, fs, args.module, args.stage, args.path, None )

if __name__ == "__main__":
    main()
