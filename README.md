# QC analysis tools for ITk pixel modules
This tools are for analyzing scan results from YARR in module QC.

Currently supporting bad pixel analysis. (ref:[https://cds.cern.ch/record/1598080/files/EndOfSummerReport.pdf](https://cds.cern.ch/record/1598080/files/EndOfSummerReport.pdf))

# Requrement
C++11 or more.<br>
ROOT, The requered version is 6.14 or more.

# Scripts
This tools consists of the following three scripts:

1. getDataFile.py (A Python script in LocalDB viewer application)
2. makeTree (C++)
3. analysis (C++)

The detailed functinality of individual files is descriped below.

## getDataFile.py
This scripts retrieve Yarr files from mongoDB and put the files to a cache directory.<br>
Then create a json file, named "info.json". Module info and pathes of the files are wrriten in the file. <br>
An example is below.<br>

info.json
```bash
{
    "datadir": "/var/tmp/okuyama/analysis-tool/20UPGR90000004_MODULEWIREBONDING",
    "module": {
        "serialNumber": "20UPGR90000004",
        "chipType": "RD53A"
    },
    "chip": [
        {
            "serialNumber": "20UPGFC0008036",
            "filepath": [
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0008036_EnMask.json",
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0008036_OccupancyMap.json",
            ]
        },
        {
            "serialNumber": "20UPGFC0007994",
            "filepath": [
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0007994_EnMask.json",
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0007994_OccupancyMap.json",
            ]
        },
        {
            "serialNumber": "20UPGFC0007972",
            "filepath": [
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0007972_EnMask.json",
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0007972_OccupancyMap.json",
            ]
        },
        {
            "serialNumber": "20UPGFC0008035",
            "filepath": [
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0008035_EnMask.json",
                "MODULEWIREBONDING/std_digitalscan_1/20UPGFC0008035_OccupancyMap.json",
            ]
        }
    ]
}
```

## makeTree
This script reads info.json created by getDataFile.py and Yarr files in the cache directry accorging to info.json.<br>
Then create a Tree file as below: <br>
![TreeFile](https://gitlab.cern.ch/YARR/utilities/localdb-qcanalysis-tools/-/raw/master/pic/Tree.png)<br>
Each result of Yarr files is stored for each leaf.
The leaf names are according to the following format. <br>
(stage name)\_(scan type)\_(# of scans)\_(filename)<br>
Each entry of the tree represents each pixel’s row, col and scan results.

## analysis
This script actually run the bad pixel analysis from the tree file created by makeTree using RDataFlame in ROOT package.<br>
Firstly we should edit "ana.json" to set analysis methods/parameters as below and input this file to the script.
```json
{
  "stages":{
    "MODULEWIREBONDING":[
      {
        "variable": "digital_occ",
        "method": "MinBound",
        "params": [1],
        "alias": "digital_dead"
      },
      {
        "variable": "threshold_map",
        "method": "GausMinMaxBound",
        "params": [5],
        "alias": "tuning_bad_threshold"
      },
    ...
    ]
  ...
  },
  "leafname":{
    "digital_occ": "std_digitalscan_1_OccupancyMap",
    "analog_occ": "std_analogscan_1_OccupancyMap",
    "Chi2_map": "std_thresholdscan_1_Chi2Map_0",
    "threshold_map": "std_thresholdscan_1_ThresholdMap_0",
    "noise_map": "std_thresholdscan_1_NoiseMap_0",
    "meantot_map": "std_totscan_1_MeanTotMap_0",
    "noise_occ": "std_noisescan_1_NoiseOccupancy"
  }
}
```
- `stages`: The production stages and lists of criteria for the stage.
- `variable`: Alias of pixel results. Mapping between the alias and Yarr files is defined in "leafname" in the bottom of this file.
- `method`: The analysis method for the criteria. Lists and definitions of individual methods are following.  
- `params`: Parameters in order to create a cut function for the criteria. 
- `alias`: Alias of the criteria .

The methods we are now implementing and setting parameters are following:
| methods             | params | corresponding cut functions   |
| -------             | ------ | ---------------------------   |
| MinBound            |  [x]   | input > x                     |
| MaxBound            |  [x]   | input < x                     |
| MinMaxBound         |  [x,y] | x< input < y                  |
| GausMinMaxBound     |  [x]   | -x\*&sigma; < input < x\*&sigma;<br>\*&sigma; is automatically calicurated in the script.|
| RemoveOneValue      |  [x]   | input ≠ x                     |
| RemoveTwoValues     |  [x,y] | input ≠ x,y                   |

Then it outputs results of bad pixel analysis as below:<br>
Number of bad pixels for each criteria:<br>
![AnalysisResult](https://gitlab.cern.ch/YARR/utilities/localdb-qcanalysis-tools/-/raw/master/pic/analysis_result.png)<br>
Distribution of bad pixels when criteria is "threshold tuning bad".<br>
![DistributionBadPixel](https://gitlab.cern.ch/YARR/utilities/localdb-qcanalysis-tools/-/raw/master/pic/tuning_bad_threshold.png)
<br>

