#include <makeTree.h>

// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// Functions
// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

json datToJson(std::string filepath) {
#ifdef DEBUG
  std::cout << "datToJson : " << filepath << std::endl;
#endif
  json j;
  std::fstream chipDataFile(filepath, std::ios::in);

  std::string type;
  std::string name;
  std::string xaxistitle, yaxistitle, zaxistitle;

  int nbinsx, nbinsy;
  double xlow, ylow;
  double xhigh, yhigh;
  int underflow, overflow;

  std::getline(chipDataFile, type);
  std::getline(chipDataFile, name);
  std::getline(chipDataFile, xaxistitle);
  std::getline(chipDataFile, yaxistitle);
  std::getline(chipDataFile, zaxistitle);

  type = type.substr(0, 7);  // EOL char kept by getline()

  if (type != "Histo1d" && type != "Histo2d" && type != "Histo3d") {
    std::cerr << "[ERROR] Something wrong with file: " << filepath << std::endl;
    j["is_null"] = true;
    return j;
  }
  j["Type"] = type;
  j["Name"] = name;
  j["x"]["AxisTitle"] = xaxistitle;
  j["y"]["AxisTitle"] = yaxistitle;
  j["z"]["AxisTitle"] = zaxistitle;

  chipDataFile >> nbinsx >> xlow >> xhigh;
  j["x"]["Bins"] = nbinsx;
  j["x"]["Low"] = xlow;
  j["x"]["High"] = xhigh;

  if (type == "Histo2d") {
    chipDataFile >> nbinsy >> ylow >> yhigh;
    j["y"]["Bins"] = nbinsy;
    j["y"]["Low"] = ylow;
    j["y"]["High"] = yhigh;
  } else {
    nbinsy = 1;
    j["y"]["Bins"] = nbinsy;
    j["y"]["Low"] = 0;
    j["y"]["High"] = 1;
  }

  chipDataFile >> underflow >> overflow;
  j["Underflow"] = underflow;
  j["Overflow"] = overflow;

  if (!chipDataFile) {
    std::cerr << "[ERROR] Something wrong with file: " << filepath << std::endl;
    j["is_null"] = true;
    return j;
  }

  // json data;
  double tmp;
  for (int row = 0; row < nbinsy; row++) {
    for (int col = 0; col < nbinsx; col++) {
      chipDataFile >> tmp;
      if (!chipDataFile) {
        std::cerr << "[ERROR] Something wrong with file: " << filepath << std::endl;
        j["is_null"] = true;
        return j;
      }
      // j["Data"][col].push_back(tmp);
      if (type == "Histo2d") {
        j["Data"][col][row] = tmp;
      } else {
        j["Data"][col] = tmp;
      }
    }
  }

  // std::ofstream ofs("data.json");
  // ofs << std::setw(4) << j;

  return j;
}

void DatHistogramer(int row, int col, std::string filepath, TH2 *h) { 
  json res = datToJson(filepath) ;
  for (int i = 0; i < col; i++) for (int j = 0; j < row; j++) h->Fill(i+1, j+1, res["Data"][i][j]); 
}

void JsonHistogramer(int row, int col, std::string filepath, TH2 *h) { 
  std::ifstream configfile(filepath, std::ios::in) ;
  json res = json::parse(configfile) ; 
  for (int i = 0; i < col; i++) for (int j = 0; j < row; j++)  h->Fill(i+1, j+1, res["Data"][i][j]); 
}

void ConfigHistogramer(int row, int col, std::string filepath, TH2 *h, std::string chip_type, std::string cname) { 
  std::ifstream configfile(filepath, std::ios::in) ;
  json res = json::parse(configfile) ; 
  for (int i = 0; i < col; i++) for (int j = 0; j < row; j++) h->Fill(i+1, j+1, res[chip_type]["PixelConfig"][i][cname][j]); 
}


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// Class: ConfigReader
// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

ConfigReader::ConfigReader(std::string filepath) {
  std::ifstream ifs(filepath);
  json j = json::parse(ifs);

  data_dir = j["datadir"];
  module_name = j["module"]["serialNumber"];
  chip_type = j["module"]["chipType"];
  chip_info = j["chip"];
  chip_num = chip_info.size();

  for (auto &path : chip_info[0]["filepath"]) {
    std::string filepath = path;
    FilePathReader filename_info(filepath);
    int type = filename_info.GetFileType();
    if (type == kDatInt || type == kJsonInt) nint_type++;
    else if (type == kDatFloat || type == kJsonFloat) nfloat_type++;
    else if (type == kJsonConfig) nint_type += 4;
    else continue;
  }
}

const std::string ConfigReader::GetDataDir() { return data_dir; }

const std::string ConfigReader::GetModuleName() { return module_name; }

const std::string ConfigReader::GetChipType() { return chip_type; }

const int ConfigReader::GetIntTypeFileNum() { return nint_type; }

const int ConfigReader::GetFloatTypeFileNum() { return nfloat_type; }

const int ConfigReader::GetChipRow() {
  if (chip_type == "RD53A") return 192;
  else if (chip_type == "FE-I4B") return 336;
  return 0;
}

const int ConfigReader::GetChipCol() {
  if (chip_type == "RD53A") return 400;
  else if (chip_type == "FE-I4B") return 80;
  return 0;
}

const int ConfigReader::GetChipNum() { return chip_num; }

const std::string ConfigReader::GetRootFileName() { return "Tree_" + module_name + ".root"; }

const std::string ConfigReader::GenFilePath(std::string filepath) { return data_dir + "/module_" + module_name + "/" + filepath; }

const json ConfigReader::GetChipInfo() { return chip_info; }


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// Class: FilePathReader
// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

FilePathReader::FilePathReader(std::string filepath) {
  sscanf(filepath.c_str(), "%[^/]/%[^/]/%[^.].%s", stage_name, scan_type, file_name, file_type);

  if (strstr(file_name, "-") == NULL) sscanf(file_name, "%[^_]_%s", chip_name, result_name);

  else sscanf(file_name, "%[^_]_%[^-]-%s", chip_name, result_name, file_number);
}

int FilePathReader::GetFileType() {
  if (strcmp(file_type, "dat") == 0) {
    if (strstr(int_type_file, result_name) != NULL) return kDatInt;
    else if (strstr(float_type_file, result_name) != NULL) return kDatFloat;
  }
  else if (strcmp(file_type, "json") == 0) {
    if (strstr(int_type_file, result_name) != NULL) return kJsonInt;
    else if (strstr(float_type_file, result_name) != NULL) return kJsonFloat;
    //else return kJsonConfig;
  }
  return -1;
}

std::string FilePathReader::GetBranchName() {
  if (strcmp(file_number, "") == 0) sprintf(branch_name, "%s_%s_%s", stage_name, scan_type, result_name);
  else sprintf(branch_name, "%s_%s_%s_%s", stage_name, scan_type, result_name, file_number);
  std::string str = branch_name;
  return str;
}


// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// main
// ....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

int main( int argc, char *argv[] ) {

  std::string config = "info.json";
  std::string o_file = "result/Tree.root";
  
  int opt;
  while ( (opt = getopt( argc, argv, "Hhc:o:" )) != -1 ){
    switch (opt){
      case 'H':
        printHelp();
        return 0;
        break;
      case 'h':
        printHelp();
        return 0;
        break;
      case 'c':
        config = std::string(optarg);
        break;
      case 'o':
        o_file = std::string(optarg);
        break;
      default:
        std::cerr << "-> Error while parsing command line parameters!" << std::endl;
        return -1;
    }
  }

  ConfigReader makeTree_info(config);
  TFile *file = TFile::Open( o_file.c_str(), "RECREATE" );

  const json chip_info = makeTree_info.GetChipInfo();
  const int nrow = makeTree_info.GetChipRow();
  const int ncol = makeTree_info.GetChipCol();
  const int nint = makeTree_info.GetIntTypeFileNum();
  const int nflo = makeTree_info.GetFloatTypeFileNum();
  const int chip_num = makeTree_info.GetChipNum();

  for (auto &cinfo : chip_info) {
 
    std::string chip_name = cinfo["serialNumber"];
    TTree *tree = new TTree( ("QCscan_" + chip_name).c_str(), ("QCscan_" + chip_name).c_str() );

    TString t_chip_name;
    Int_t t_row;
    Int_t t_col;
    Int_t t_int_val[nint];
    Float_t t_float_val[nflo];
    TH2I *h_int[nint];
    TH2F *h_float[nflo];
  
    tree -> Branch( "row", &t_row );
    tree -> Branch( "col", &t_col );

    int nh_int = 0;
    int nh_float = 0;
    for (auto &part_path : cinfo["filepath"]) {
      FilePathReader filename_info(part_path); //Read a part file path
      int type = filename_info.GetFileType(); //Get file type from the part file path (int or float or config)
      std::string branch_name = filename_info.GetBranchName(); //Get branch name from the part file path
      std::string filepath = makeTree_info.GenFilePath(part_path); //Generate a full file path
      if ( type == kDatInt || type == kJsonInt) {
        tree -> Branch(branch_name.c_str(), &t_int_val[nh_int]);
        h_int[nh_int] = new TH2I((chip_name + branch_name).c_str(), (chip_name + branch_name).c_str(), ncol, 0, ncol+0.5, nrow, 0, nrow+0.5);
        if(type == kDatInt) DatHistogramer(nrow, ncol, filepath, h_int[nh_int]);
        if(type == kJsonInt) JsonHistogramer(nrow, ncol, filepath, h_int[nh_int]);
        nh_int++;
      } 
      else if ( type == kDatFloat || type == kJsonFloat) {
        tree -> Branch(branch_name.c_str(), &t_float_val[nh_float]);
        h_float[nh_float] = new TH2F((chip_name + branch_name).c_str(), (chip_name + branch_name).c_str(), ncol, 0, ncol+0.5, nrow, 0, nrow+0.5);
        if(type == kDatFloat) DatHistogramer(nrow, ncol, filepath, h_float[nh_float]);
        if(type == kJsonFloat) JsonHistogramer(nrow, ncol, filepath, h_float[nh_float]);
        nh_float++;
      } 
//      else if ( type == kJsonConfig ) {
//        for (auto &cname : config_value_name) {
//          std::string this_branch_name = branch_name + "_" + cname;
//          tree -> Branch(this_branch_name.c_str(), &t_int_val[nh_int]);
//          h_int[nh_int] = new TH2I(this_branch_name.c_str(), this_branch_name.c_str(), ncol, 0, ncol+0.5, nrow, 0, nrow+0.5);
//          ConfigHistogramer(nrow, ncol, filepath, h_int[nh_int], makeTree_info.GetChipType(), cname);
//          nh_int++;
//        }
//      }
      else
        continue;
    }

    for (int i = 0; i < ncol; i++) {
      for (int j = 0; j < nrow; j++) {
        t_col = (i+1);
        t_row = (j+1);
        for (int k = 0; k < nint; k++) t_int_val[k] = h_int[k] -> GetBinContent( i+1, j+1 );
        for (int l = 0; l < nflo; l++) t_float_val[l] = h_float[l] -> GetBinContent( i+1, j+1 );
        tree->Fill();
      }
    }

    tree->Write();
  }
  
  file->Close();
}

void printHelp() {
    std::cout << "Help:" << std::endl;
    std::cout << " -h/-H: Shows this." << std::endl;
    std::cout << " -c <file> : Input config file path for makig tree(default:'info.json')" << std::endl;
    std::cout << " -o <file> : Input output file's name(default:result/Tree.root)" << std::endl;
}
